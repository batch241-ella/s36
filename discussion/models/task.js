// Create the Schema, model, and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}

});

// creates a collection with the name of the model in plural form
// "Task" model -> "tasks" collection
module.exports = mongoose.model("Task", taskSchema);
// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files